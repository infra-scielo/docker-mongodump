#!/bin/bash

set -e

echo "Job started: $(date)"

DATE=$(date +%Y%m%d_%H%M%S)
FILE="/backup/backup-$DATE.tar.gz"
RESERVE_NUM=${RESERVES:-7}

# exec command
start=$(date +"%Y-%m-%d %H:%M:%S")
echo "$start: start database dump..."

cd /backup
mkdir -p dump
mongodump -h $MONGO_HOST -p $MONGO_PORT -u $MONGO_USER -p $MONGO_PWD --out dump
tar -zcvf $FILE dump/
rm -rf dump/*

RET=$?
end=$(date +"%Y-%m-%d %H:%M:%S")

# check result and cleanup old file
if [ $RET -ne 0 ]; then
  echo "$end: backup error: exit code $RET"
  rm -rf /backup/dump/*
  exit $RET
else
  echo "$end: backup successful."
  cd /backup && ls -lrt | head -n -$RESERVE_NUM | awk '{print $9}' | xargs rm -f
  exit 0
fi

echo "Job finished: $(date)"
